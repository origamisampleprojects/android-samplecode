package com.assessment.origami.Models;

/**
 * Created by OrigamiStudios on 03/03/16.
 * The following code is solely a property of OrigamiStudios Pvt Ltd.
 * Any usage outside the logical domains of learning will be considered violation.
 */
public class User {

    /**
     * Declaring all private data members
     */
    private String firstName;
    private String lastName;



    /**
     * Constructor
     */
    public  User(String completeName)
    {
        /** Remove all non visible characters and commas */
        completeName = completeName.replaceAll("\\s+","");
        if(completeName.contains(",")){
            String[] separateName = completeName.split(",");
            String fn = separateName[0];
            String ln = separateName[1];

            /** converting first letter to upper case and applying it to setters */
             this.setName(ln.substring(0, 1).toUpperCase() + ln.substring(1),fn.substring(0, 1).toUpperCase() + fn.substring(1));
        } else {

            /** No comma dilimeter found, it means its a single string */
            this.setName((completeName.substring(0, 1).toUpperCase().toString() + completeName.substring(1).toString()),"");
        }
    }

    /**
     * Setting FirstName and LastName
     */
    public void setName(String lastName,String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    /**
     * Getting FirstName and LastName
     */
    public String getName() {
        return lastName+", "+firstName;
    }

    /**
     * Getting FUllName
     */
    @Override
    public String toString() {
        return getName();
    }
}
