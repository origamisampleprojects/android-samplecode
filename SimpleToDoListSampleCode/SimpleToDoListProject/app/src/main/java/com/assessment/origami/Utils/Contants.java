package com.assessment.origami.Utils;

import com.assessment.origami.Models.User;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mr on 03/03/16.
 * The following code is solely a property of OrigamiStudios Pvt Ltd.
 * Any usage outside the logical domains of learning will be considered violation.
 */
public class Contants {

    /**
     * This Method Populates List with Pre-Defined String of First Name and Last Name
     */
    public static List<User> addDataToList()
    {
        List<User> dummyDataList = new ArrayList();
        dummyDataList.add(new User("Ahmad, Moeed"));
        dummyDataList.add(new User("Mehroze, Yaqoob"));
        dummyDataList.add(new User("Taha, Sheikh"));
        dummyDataList.add(new User("Akbar, Farooq"));
        dummyDataList.add(new User("Abbasi, Usman"));

        return dummyDataList;
        // To Add More Dummy Data, Simply Add String in Dummy Data List here.
    }





}
