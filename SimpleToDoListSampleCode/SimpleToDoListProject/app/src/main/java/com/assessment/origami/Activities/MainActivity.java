package com.assessment.origami.Activities;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.assessment.origami.Models.User;
import com.assessment.origami.R;
import com.assessment.origami.Utils.Contants;
import java.util.List;

/**
 * Created by OrigamiStudios on 03/03/16.
 * The following code is solely a property of OrigamiStudios Pvt Ltd.
 * Any usage outside the logical domains of learning will be considered violation.
 */
public class MainActivity extends Activity {

    /**
     * Declaring private global variables
     */
    private List<User> userItems;
    private ArrayAdapter<User> itemsAdapter;
    private ListView lvItems;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // getting references of widgets
        lvItems = (ListView) findViewById(R.id.lvItems);
        // Initialising array list
        userItems = Contants.addDataToList();

        // setting adapter
        itemsAdapter = new ArrayAdapter<User>(this,
                android.R.layout.simple_list_item_1, userItems);
        lvItems.setAdapter(itemsAdapter);

        // Setup remove listener method call
        setupListViewListener();

    }



    /**
     * This method is primarily for Deleting an Item from ListView Upon Long Press
     */
    private void setupListViewListener() {

        // Adding Listener to ListView
        lvItems.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapter,
                                                   View item, int pos, long id) {
                        userItems.remove(pos);

                        // Refresh the adapter
                        itemsAdapter.notifyDataSetChanged();
                        return true;
                    }

                });
    }



    /**
     *  This is a Callback Method of Plus Sign Button
     */
    public void onAddItemClicked(View v) {
        addMoreItemPopup();
    }


    /**
     *  This method will bring a popup that asks user
     * to enter new entry of first name and last name
     */
    private void addMoreItemPopup()
    {
        // Creating a Build Class
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Setting Title of Popup
        builder.setTitle("Dev Crew");

        // Setting Message
        builder.setMessage("Add New Name");

        // creating a EditText input field at run time
        final EditText inputField = new EditText(this);

        // Setting Hint of Text to be Added
        inputField.setHint("Your Good Name");

        // Assigning View of EditText to Popup
        builder.setView(inputField);

        // Adding Buttons to Popup
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                /**
                 * Method that will Process the String Entered by the User
                 */
                addNewName(inputField.getText().toString());
                inputField.setText("");

            }
        });

        // Adding Negative Button to Cancel the Dialog
        builder.setNegativeButton("Cancel", null);

        builder.create().show();
    }


    /**
     *  Method that will Process the String Entered by User
     */
    private  void addNewName(String itemText)
    {

        // If no name is entered
        if (itemText.length() <= 0) {
            Toast.makeText(getApplicationContext(), "No Name Entered", Toast.LENGTH_LONG).show();
            return;
        }

        // Creating a New Object of User with Entered Name
        User _userNew = new User(itemText);

        // Adding the User Object to ListView
        itemsAdapter.add(_userNew);
    }

}

